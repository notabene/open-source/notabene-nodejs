import PIIsdk, { PIIEncryptionMethod } from '@notabene/pii-sdk';
import { AxiosInstance } from 'axios';
import { ethers } from 'ethers';
import { NotabeneConfig } from '../notabene';
import TrustFramework from './trustframework';

export enum TransactionGroup {
  INBOX = 'INBOX',
  ALL = 'ALL',
}

export enum TransactionDirection {
  INCOMING = 'incoming',
  OUTGOING = 'outgoing',
}

export enum TransactionType {
  TRAVEL_RULE,
  BELOW_THRESHOLD,
  NON_CUSTODIAL,
  UNKNOWN,
}

export type OwnershipProof = {
  type: string;
  proof: string;
};

export type CreateTransaction = {
  transactionAsset: string;
  transactionAmount: string;
  originatorDid?: string;
  beneficiaryDid?: string;
  originatorVASPdid: string;
  beneficiaryVASPdid?: string;
  beneficiaryVASPname?: string;
  transactionBlockchainInfo?: {
    origin?: string;
    destination?: string;
    txHash?: string;
  };
  originator?: any;
  beneficiary?: any;
  pii?: any;
  pii_url?: string;
  protocol?: ['TRLight', 'TRP', 'OpenVASP'];
  notificationEmail?: string;
  originatorProof?: OwnershipProof;
  beneficiaryProof?: OwnershipProof;
  skipBeneficiaryDataValidation?: boolean;
};

export type UpdateTransaction = {
  id: string;
  beneficiaryVASPdid?: string;
  originatorVASPdid?: string;
  beneficiary?: any;
  originator?: any;
  txHash?: string;
  origin?: string;
  destination?: string;
  notificationEmail?: string;
  pii?: any;
  pii_url?: string;
};

export type ValidateTransaction = {
  transactionAsset: string;
  destination: string;
  transactionAmount: string;
  originatorVASPdid: string;
  originatorEqualsBeneficiary: boolean;
  beneficiaryVASPdid?: string;
  beneficiaryVASPname?: string;
  beneficiaryName?: string;
  beneficiaryAccountNumber?: string;
  beneficiaryAddress?: Address;
  beneficiaryProof?: OwnershipProof;
  travelRuleBehavior?: boolean;
};

export enum AddressTypeCode {
  HOME = 'HOME',
  BIZZ = 'BIZZ',
  GEOG = 'GEOG',
}

export type Address = {
  addressType?: AddressTypeCode;
  department?: string;
  subDepartment?: string;
  streetName?: string;
  buildingNumber?: string;
  buildingName?: string;
  floor?: string;
  postBox?: string;
  room?: string;
  postCode?: string;
  townName?: string;
  townLocationName?: string;
  districtName?: string;
  countrySubDivision?: string;
  addressLine?: string[];
  country?: string;
};

export default class Transaction {
  private http: AxiosInstance;
  private toolset: PIIsdk;
  private trustframework: TrustFramework;
  private config: NotabeneConfig;

  constructor(
    http: AxiosInstance,
    toolset: PIIsdk,
    trustframework: TrustFramework,
    config: NotabeneConfig
  ) {
    this.http = http;
    this.toolset = toolset;
    this.trustframework = trustframework;
    this.config = config;
  }

  /**
   * Get a transaction by ID
   *
   * @external https://doc.notabene.id/#operation/txInfo
   *
   * @param id The ID of the transaction
   * @returns A transaction
   */
  public async get(id: string, jsonDIDKey?: string) {
    const transaction = (await this.http.get('/tx/info', {
      params: {
        id,
      },
    })) as any;

    if (transaction.pii && jsonDIDKey) {
      try {
        const keypair = JSON.parse(jsonDIDKey);
        // try to get decrypted PII
        transaction.ivms101 = await this.toolset.getPIIObject(
          transaction.pii,
          transaction.pii_url,
          keypair
        );
      } catch (err: any) {
        // if decryption fails don't throw (because it is E2E encrypted)
        if (
          !(err + '').includes(
            'unable to decrypt DIDComm message with any of the locally managed keys'
          )
        ) {
          throw err;
        }
      }
    }

    return transaction;
  }

  /**
   * Get a list of transactions
   *
   * @external https://doc.notabene.id/#operation/txList
   *
   * @param did VASPDid to get the list of transactions for
   * @param page The pagination page
   * @param perPage The number of results per page
   * @param group The group of transactions to show e.g. INBOX or ALL
   * @param direction A direction to filter transactions by e.g. INCOMING or OUTGOING
   * @param asset A blockchain asset to filter transactions for
   * @param type The type of transaction to filter by e.g. TRAVEL_RULE, BELOW_THRESHOLD etc
   * @param search A search query to filter transactions by
   * @param sort A sort direction for the results
   * @returns A list of transactions
   */
  public async list(
    did: string,
    group: TransactionGroup = TransactionGroup.INBOX,
    page?: number,
    perPage?: number,
    direction?: TransactionDirection,
    transactionAsset?: string,
    type?: TransactionType,
    search?: string,
    sort?: string,
    decrypt?: boolean
  ) {
    return this.http.get('/tx/list', {
      params: {
        vaspDID: did,
        sort,
        page,
        resultsPerPage: perPage,
        txGroup: group,
        transactionAsset,
        transactionType: type,
        q: search,
        txDirection: direction,
        decrypt,
      },
    });
  }

  /**
   * Create a new transaction
   *
   * @external https://doc.notabene.id/#operation/txCreate
   *
   * @param transaction
   * @returns The created transaction
   */
  public async create(
    transaction: CreateTransaction,
    jsonDIDKey?: string,
    toNotabene?: boolean
  ) {
    if (jsonDIDKey) {
      const pii = transaction.pii || {
        originator: transaction.originator,
        beneficiary: transaction.beneficiary,
      };

      const counterpartyDIDKey: string = transaction.beneficiaryVASPdid
        ? (
            (await this.trustframework.get(
              transaction.beneficiaryVASPdid as string,
              ['pii_didkey']
            )) as any
          ).pii_didkey
        : undefined;

      const keypair = JSON.parse(jsonDIDKey);

      const piiIvms = await this.toolset.generatePIIField({
        pii,
        originatorVASPdid: transaction.originatorVASPdid,
        beneficiaryVASPdid: transaction.beneficiaryVASPdid,
        counterpartyDIDKey,
        keypair,
        senderDIDKey: keypair.did,
        encryptionMethod: toNotabene
          ? PIIEncryptionMethod.HYBRID
          : PIIEncryptionMethod.END_2_END,
      });
      transaction.pii = piiIvms;
      transaction.pii_url = transaction.pii_url || this.toolset.config.piiURL;
    }

    return await this.http.post('/tx/create', transaction);
  }

  /**
   * Partially validate a new transaction
   *
   * @external https://doc.notabene.id/#operation/txValidate
   *
   * @param transaction
   * @returns The validation results
   */
  public async validate(transaction: ValidateTransaction) {
    return await this.http.post('/tx/validate', transaction);
  }

  /**
   * Validate a new transaction
   *
   * @external https://doc.notabene.id/#operation/txValidateFull
   *
   * @param transaction
   * @returns The validation results
   */
  public async validateFull(transaction: CreateTransaction) {
    return await this.http.post('/tx/validate/full', transaction);
  }

  /**
   * Create a new transaction
   *
   * @external https://doc.notabene.id/#operation/txCreate
   *
   * @param transaction
   * @returns The created transaction
   */
  public async update(
    transaction: UpdateTransaction,
    jsonDIDKey?: string,
    toNotabene?: boolean
  ) {
    if (jsonDIDKey) {
      const pii = transaction.pii || {
        originator: transaction.originator,
        beneficiary: transaction.beneficiary,
      };

      const counterpartyDIDKey: string = (
        (await this.trustframework.get(
          (transaction.beneficiaryVASPdid ||
            transaction.originatorVASPdid) as string,
          ['pii_didkey']
        )) as any
      ).pii_didkey;
      if (!counterpartyDIDKey || !counterpartyDIDKey.startsWith('did:key:'))
        throw new Error('Failed to obtain DIDKey of counterparty');

      const keypair = JSON.parse(jsonDIDKey);
      const piiIvms = await this.toolset.generatePIIField({
        pii,
        originatorVASPdid: transaction.originatorVASPdid,
        beneficiaryVASPdid: transaction.beneficiaryVASPdid,
        counterpartyDIDKey,
        keypair,
        senderDIDKey: keypair.did,
        encryptionMethod: toNotabene
          ? PIIEncryptionMethod.HYBRID
          : PIIEncryptionMethod.END_2_END,
      });
      transaction.pii = piiIvms;
      transaction.pii_url = transaction.pii_url || this.toolset.config.piiURL;
    }
    return await this.http.post('/tx/update', transaction);
  }

  /**
   * Approve an outgoing transaction
   *
   * @external https://doc.notabene.id/#operation/txApprove
   *
   * @param id The ID of the transaction
   * @returns The approved transaction
   */
  public async approve(id: string) {
    return this.http.post(
      '/tx/approve',
      {},
      {
        params: {
          id,
        },
      }
    );
  }

  /**
   * Cancel a transaction
   *
   * @external https://doc.notabene.id/#operation/txCancel
   *
   * @param id The ID of the transaction
   * @param reason The reason why the transaction was cancelled
   * @returns The cancelled transaction
   */
  public async cancel(id: string, reason?: string) {
    return this.http.post('/tx/cancel', {
      id,
      reason,
    });
  }

  /**
   * Confirms a transaction
   *
   * @external https://doc.notabene.id/#operation/txConfirm
   *
   * @param id The ID of the transaction
   * @returns The confirmed transaction
   */
  public async confirm(id: string) {
    return this.http.post(
      '/tx/confirm',
      {},
      {
        params: {
          id,
        },
      }
    );
  }

  /**
   * Noifies the originator VASP of an incoming transaction on the blockchain
   *
   * @external https://doc.notabene.id/#operation/txNotify
   *
   * @param hash The transaction hash
   * @param amount The transaction amount
   * @param asset The transaction asset
   * @param beneficiaryVASPdid The DID of the beneficiary VASP
   * @param destination The address of the destination
   * @param origin The address of the originator
   * @param customerRef The customer reference
   * @returns
   */
  public async notify(
    txHash: string,
    transactionAmount: string,
    transactionAsset: string,
    beneficiaryVASPdid: string,
    destination: string,
    origin?: string,
    customerRef?: string
  ) {
    return this.http.post('/tx/notify', {
      txHash,
      transactionAmount,
      transactionAsset,
      beneficiaryVASPdid,
      destination,
      origin,
      customerRef,
    });
  }

  /**
   * Reject a transaction
   *
   * @external https://doc.notabene.id/#operation/txReject
   *
   * @param id The ID of the transaction
   * @param reason The reason the transaction was rejected
   * @returns The rejected transaction
   */
  public async reject(id: string, reason?: string) {
    return this.http.post('/tx/reject', {
      id,
      reason,
    });
  }

  /**
   * Mark transaction as not ready to respond
   *
   * @external https://doc.notabene.id/#operation/txNotReady
   *
   * @param id The ID of the transaction
   * @param reason The reason
   * @returns The transaction
   */
  public async notReady(id: string, reason?: string) {
    return this.http.post('/tx/notReady', {
      id,
      reason,
    });
  }

  /**
   * Accept a transaction
   *
   * @external https://doc.notabene.id/#operation/txAccept
   *
   * @param id The ID of the transaction
   * @returns The accepted transaction
   */
  public async accept(id: string) {
    return this.http.post(
      '/tx/accept',
      {},
      {
        params: {
          id,
        },
      }
    );
  }

  /**
   * Decline a transaction
   *
   * @external https://doc.notabene.id/#operation/txDecline
   *
   * @param id The ID of the transaction
   * @param reason The reason why the transaction was declined
   * @returns The declined transaction
   */
  public async decline(id: string, reason?: string) {
    return this.http.post('/tx/decline', {
      id,
      reason,
    });
  }

  /**
   * Redirect a transaction from one VASP to another
   *
   * @external https://doc.notabene.id/#operation/txRedirect
   *
   * @param id The ID of the transaction
   * @param to The DID of the VASP to redirect to
   * @returns The redirected transaction
   */
  public async redirect(id: string, to: string) {
    return this.http.post(
      '/tx/redirect',
      {},
      {
        params: {
          id,
          to,
        },
      }
    );
  }

  /**
   * Verify a signature (Used for beneficiaryOwnerProof)
   *
   * @param signature Signed message
   * @param message Message
   * @param address Address of the signer
   * @returns True if the `address` matches with the signed proof. False otherwise.
   */
  public async verifyMessage(
    signature: string,
    message: string,
    address: string
  ): Promise<boolean> {
    return ethers.verifyMessage(message, signature) === address;
  }
}
