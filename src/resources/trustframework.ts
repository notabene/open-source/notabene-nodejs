import { AxiosInstance } from 'axios';

export default class TrustFramework {
  private http: AxiosInstance;

  constructor(http: AxiosInstance) {
    this.http = http;
  }

  public async get(did: string, fields: string[]) {
    return await this.http.get(`/tf/simple/vasps/${did}`, {
      params: {
        fields: fields.join(','),
      },
    });
  }

  public async list(
    fields: string[],
    page = 0,
    perPage = 10,
    search?: string,
    order?: any
  ) {
    return await this.http.get('/tf/simple/vasps', {
      params: {
        q: search,
        fields: fields.join(','),
        page,
        per_page: perPage,
        order,
      },
    });
  }

  public async update(did: string, fields: any[]) {
    return await this.http.post('/tf/vasps/update', { fields, did });
  }

  public async getSettings(did: string) {
    return await this.http.get(`/tf/vasps/${did}/getSettings`);
  }

  public async updateSettings(did: string, settings: VaspSettingsValue) {
    return await this.http.put(`/tf/vasps/${did}/settings`, settings);
  }
}

export interface VaspSettingsValue {
  autoconfirm?: boolean;
  statusToProcessBlockchain?: StatusToProcessBlockchain | null;
  vaspDiscoverability?: boolean;
}

export enum StatusToProcessBlockchain {
  CREATED = 'CREATED',
  SENT = 'SENT',
  ACK = 'ACK',
  ACCEPTED = 'ACCEPTED',
}
