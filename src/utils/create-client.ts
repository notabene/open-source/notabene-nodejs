import oauth from 'axios-oauth-client';
import axios from 'axios';
import tokenProvider from 'axios-token-interceptor';
import { NotabeneConfig } from '../notabene';
import freezeSys from './freeze-object';

const tokenInterceptor = (config: NotabeneConfig) => {
  return () =>
    getToken(
      config.clientId,
      config.clientSecret,
      config.notabeneUserAgentHeader,
      config.authURL,
      config.audience
    );
};

export const getToken = async (
  clientId: string,
  clientSecret: string,
  notabeneUserAgentHeader?: string,
  authURL = 'https://auth.notabene.id',
  audience = 'https://api.notabene.id'
) => {
  const resp = await axios.post(
    `${authURL}/oauth/token`,
    {
      grant_type: 'client_credentials',
      client_id: clientId,
      client_secret: clientSecret,
      audience: audience,
    },
    {
      headers: {
        'Content-Type': 'application/json',
        'X-User-Agent': notabeneUserAgentHeader || 'notabene/sdk',
      },
      method: 'POST',
    }
  );
  return resp.data;
};

const errorInterceptor = (error: any) => {
  return Promise.reject({
    req: {
      url: '' + error?.response?.config?.baseURL + error?.response?.config?.url,
      method: error?.request?.method,
    },
    status: error?.response?.status,
    statusText: error?.response?.statusText,
    err: JSON.stringify(
      error?.response?.data?.err || error?.response?.data || error
    ),
  });
};

axios.interceptors.request.use(
  (res: any) => res,
  (error: any) => errorInterceptor(error)
);
axios.interceptors.response.use(
  (res: any) => res,
  (error: any) => errorInterceptor(error)
);

export function createClient(config: NotabeneConfig) {
  const client = axios.create({
    baseURL: config.baseURL,
    headers: {
      'X-Notabene-SDK': 'notabene-sdk',
    },
  });

  client.interceptors.request.use(
    (res: any) => res,
    (error: any) => errorInterceptor(error)
  );
  client.interceptors.response.use(
    (res: any) => res,
    (error: any) => errorInterceptor(error)
  );

  client.interceptors.response.use((response: any) => {
    return freezeSys(response.data);
  });

  client.interceptors.request.use(
    oauth.interceptor(tokenProvider, tokenInterceptor(config))
  );

  return client;
}
