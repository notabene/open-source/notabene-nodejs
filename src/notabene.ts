import PIIsdk from '@notabene/pii-sdk';
import { AxiosInstance } from 'axios';
import Transaction from './resources/transaction';
import TrustFramework from './resources/trustframework';
import { createClient } from './utils/create-client';

export type NotabeneConfig = {
  clientId: string;
  clientSecret: string;
  authURL?: string;
  audience?: string;
  audiencePII?: string;

  baseURL?: string;
  baseURLPII?: string;

  notabeneUserAgentHeader?: string;
};

const defaults: Partial<NotabeneConfig> = {
  baseURL: 'https://api.notabene.id',
  baseURLPII: 'https://pii.notabene.id',
  audience: 'https://api.notabene.id',
  audiencePII: 'https://pii.notabene.id',
  authURL: 'https://auth.notabene.id',
};

export class Notabene {
  public http: AxiosInstance;
  public transaction: Transaction;
  public trustFramework: TrustFramework;
  public toolset: PIIsdk;

  constructor(config: NotabeneConfig) {
    const configWithDefaults = { ...defaults, ...config };
    this.http = createClient(configWithDefaults);

    this.toolset = new PIIsdk({
      piiURL: configWithDefaults.baseURLPII,
      audience: configWithDefaults.audiencePII,
      clientId: configWithDefaults.clientId,
      clientSecret: configWithDefaults.clientSecret,
      authURL: configWithDefaults.authURL,
    });

    this.trustFramework = new TrustFramework(this.http);

    this.transaction = new Transaction(
      this.http,
      this.toolset,
      this.trustFramework,
      configWithDefaults
    );
  }

  async customerToken(vaspDID: string, customerRef: string) {
    return this.http.post('/auth/customerToken', {
      vaspDID,
      customerRef,
    });
  }
}
